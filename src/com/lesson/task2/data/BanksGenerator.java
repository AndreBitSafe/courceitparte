package com.lesson.task2.data;

import com.lesson.task2.model.Bank;
import com.lesson.task2.model.Currency;

public final class BanksGenerator {

    private BanksGenerator() {
    }

    public static Bank[] generateBanks(){
        Bank[] banks = new Bank[5];
        {
            Currency[] rates = {
                    new Currency("USD",27.30f,27.90f),
                    new Currency("EUR",29.10f,30.10f),
                    new Currency("RUB",0.25f,0.36f),
            };
            banks[0] = new Bank("Банк Конкорд", rates);
        }
        {
            Currency[] rates = {
                    new Currency("USD",27.35f,27.98f),
                    new Currency("EUR",29.20f,30.15f),
                    new Currency("RUB",0.25f,0.37f),
            };
            banks[1] = new Bank("Полтава-Банк", rates);
        }
        {
            Currency[] rates = {
                    new Currency("USD",27.05f,28.10f),
                    new Currency("EUR",29.50f,30.00f),
                    new Currency("RUB",0.35f,0.46f),
            };
            banks[2] = new Bank("Альфа-Банк", rates);
        }
        {
            Currency[] rates = {
                    new Currency("USD",27.64f,27.99f),
                    new Currency("EUR",29.10f,30.20f),
                    new Currency("RUB",0.27f,0.35f),
            };
            banks[3] = new Bank("БТА Банк", rates);
        }
        {
            Currency[] rates = {
                    new Currency("USD",27.35f,27.95f),
                    new Currency("EUR",29.17f,30.18f),
                    new Currency("RUB",0.24f,0.35f),
            };
            banks[4] = new Bank("Сбербанк", rates);
        }
        return banks;
    }
}
