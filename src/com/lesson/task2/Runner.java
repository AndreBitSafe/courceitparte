package com.lesson.task2;

import com.lesson.task2.data.BanksGenerator;
import com.lesson.task2.model.BanksDataBase;

import java.util.Scanner;

public class Runner {
    public void start (){
        BanksDataBase banksDataBase = new BanksDataBase(BanksGenerator.generateBanks());

        System.out.println("Все банки: ");
        banksDataBase.printAllInfo();

        Scanner in = new Scanner(System.in);
        System.out.println("Введите банк, валюту и операцию(покупка/продажа) разделяя слова запятой(\", \"):");
        String inString = in.nextLine();

        System.out.print("Введите количество денег каторые нужно конвертировать: ");
        int amount = Integer.parseInt(in.nextLine());

        banksDataBase.printConvertInfo(inString.split(", "), amount);
    }
}
