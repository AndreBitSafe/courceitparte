package com.lesson.task2.model;

public class Bank {
    public static final int BUYING_OPERATION = 0;
    public static final int SELLING_OPERATION = 1;

    private String name;
    private Currency[] currencies;

    public Bank(String name, Currency[] currency) {
        this.name = name;
        this.currencies = currency;
    }

    public String getAllInfoAboutTheBank() {
        StringBuilder infString = new StringBuilder("--> " + this.name + "\n");
        infString.append("\tКурсы валют:\n");
        String formatStr = "\t%s: Покупка - %.02f;\t Продажа - %.02f\n";
        for (Currency rate : this.currencies) {
            infString.append(String.format(formatStr, rate.getNameAbbreviation(), rate.getCourseBuy(), rate.getCourseSelling()));
        }
        return infString.toString();
    }


    public static Bank findBank (String inName, Bank[] banks){
        for (Bank bank : banks) {
            if (bank.name.equalsIgnoreCase(inName)){
                return bank;
            }
        }
        return null;
    }

    public float convert(String currency, int amount, int operation) {
        for (Currency thisCurrency : this.currencies) {
            if (thisCurrency.equalsName(currency)){
                switch (operation){
                    case BUYING_OPERATION:
                        return amount / thisCurrency.getCourseSelling();
                    case SELLING_OPERATION:
                        return amount * thisCurrency.getCourseBuy();
                    default:
                        System.out.println("Операция указана не верно");
                        return 0;
                }
            }
        }
        return 0;
    }
}
