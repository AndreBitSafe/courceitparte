package com.lesson.task2.model;

public class Currency {
    private String nameAbbreviation;
    private float courseBuy;
    private float courseSelling;

    public Currency(String nameAbbreviation, float courseBuy, float courseSelling) {
        this.nameAbbreviation = nameAbbreviation;
        this.courseBuy = courseBuy;
        this.courseSelling = courseSelling;
    }

    public String getNameAbbreviation() {
        return nameAbbreviation;
    }

    public float getCourseBuy() {
        return courseBuy;
    }

    public float getCourseSelling() {
        return courseSelling;
    }

    public boolean equalsName(String curr) {
        if (curr.equalsIgnoreCase(this.nameAbbreviation)){
            return true;
        }
        return false;
    }
}
