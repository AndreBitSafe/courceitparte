package com.lesson.task2.model;

public class BanksDataBase {

    private Bank[] banks;

    public BanksDataBase(Bank[] banks) {
        this.banks = banks;
    }

    public void printAllInfo (){
        for (Bank bank : banks) {
            System.out.println(bank.getAllInfoAboutTheBank());
        }
    }

    public void printConvertInfo(String[] bankNameCurrencyOperation, int amount) {
        String bankName = bankNameCurrencyOperation[0];
        String currency = bankNameCurrencyOperation[1];
        String operation = bankNameCurrencyOperation[2];
        String uah = "UAH";
        String formatString = "%d %s в %s => %.02f.";
        float convertAmount;

        Bank bank = Bank.findBank(bankName, banks);
        if (bank == null){
            System.out.println("Банк не найден!!!");
            return;
        }

        if (operation.equalsIgnoreCase("покупка")){
            convertAmount = bank.convert(currency, amount, Bank.BUYING_OPERATION);
            System.out.println(String.format(formatString,amount,uah,currency,convertAmount));
        }else{
            convertAmount = bank.convert(currency, amount, Bank.SELLING_OPERATION);
            System.out.println(String.format(formatString,amount,currency,uah,convertAmount));
        }
    }//сбербанк, eur, покупка
}
