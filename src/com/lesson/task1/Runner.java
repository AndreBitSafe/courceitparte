package com.lesson.task1;

import com.lesson.task1.data.Generator;
import com.lesson.task1.model.Library;

import java.util.Scanner;

public class Runner {
    public void start() {
        Library library = new Library(Generator.generateBooks());
        library.findOldestBook();

        Scanner in = new Scanner(System.in);

        System.out.print("Введите автора, книгу которого нужно найти: ");
        String inString = in.nextLine();

        library.findAuthorsBook(inString);

        System.out.print("\nВведите максимальный год издания для выводимых книг: ");
        int findPublishingYear = Integer.parseInt(in.nextLine());

        library.findOlderBooks(findPublishingYear);
    }
}
