package com.lesson.task1.data;

import com.lesson.task1.model.Book;
import com.lesson.task1.model.PublishingInfo;

public final class Generator {
    private Generator() {
    }

    public static Book[] generateBooks() {
        Book[] books = new Book[6];
        {
            PublishingInfo publishingInfo =
                    new PublishingInfo("Диалектика", 2019, new String[]{"Сантану Паттанаяк"});
            books[0] = new Book("Глубокое обучение и TensorFlow для профессионалов", publishingInfo);
        }
        {
            PublishingInfo publishingInfo =
                    new PublishingInfo("Диалектика", 2020, new String[]{"Джон Пол Мюллер", "Дебби Валковски"});
            books[1] = new Book("Python для чайников", publishingInfo);
        }
        {
            PublishingInfo publishingInfo =
                    new PublishingInfo("Символ-Плюс", 2020, new String[]{"Мартин Фаулер"});
            books[2] = new Book("Рефакторинг кода на JavaScript: улучшение проекта существующего кода",
                    publishingInfo);
        }
        {
            PublishingInfo publishingInfo =
                    new PublishingInfo("Диалектика", 2018, new String[]{"Герберт Шилдт"});
            books[3] = new Book("Java. Полное руководство, 10-е издание",
                    publishingInfo);
        }
        {
            PublishingInfo publishingInfo =
                    new PublishingInfo("Альфа-книга", 2019, new String[]{"Роберт Лигуори", "Патрисия Лигуори"});
            books[4] = new Book("Java 8. Карманный справочник",
                    publishingInfo);
        }
        {
            PublishingInfo publishingInfo =
                    new PublishingInfo("Ранок", 2009,
                            new String[]{"А. Г. Мерзляк", "Д. А. Номировский", "В. Б. Полонский", "М. С. Якир"});
            books[5] = new Book("Учебник Геометрия 11 класс",
                    publishingInfo);
        }
        return books;
    }

}
