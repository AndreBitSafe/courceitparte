package com.lesson.task1.model;

public class Book {
    private String name;
    private PublishingInfo publishingInfo;

    public Book(String name, PublishingInfo publishingInfo) {
        this.name = name;
        this.publishingInfo = publishingInfo;
    }

    public String getName() {
        return name;
    }

    public PublishingInfo getInfo() {
        return this.publishingInfo;
    }

    public void tryToFindAuthor(String author) {
        for (String authorStr : publishingInfo.getAuthors()) {
            if (authorStr.equalsIgnoreCase(author)){
                System.out.println(this);
            }
        }
    }

    @Override
    public String toString() {

        return "--> Название: " + this.name + ";\n" +
                publishingInfo;



    }
}
