package com.lesson.task1.model;

public class Library {
    private Book[] books;

    public Library(Book[] books) {
        this.books = books;
    }

    public void findOldestBook () {
        Book book = books[0];
        for (Book iteratorBook : books) {
            if (iteratorBook.getInfo().getPublishingYear() < book.getInfo().getPublishingYear()){
                book = iteratorBook;
            }
        }
        System.out.println("Самая старая книга: " + book.getName());
    }

    public void findAuthorsBook(String author){
        for (Book book : books) {
            book.tryToFindAuthor(author);
        }
    }

    public void findOlderBooks (int maxYear){
        for (Book book : books) {
            if (book.getInfo().getPublishingYear() < maxYear){
                System.out.println(book);
            }
        }
    }
}
