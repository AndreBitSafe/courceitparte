package com.lesson.task1.model;

public class PublishingInfo {

    private String organizationName;
    private int publishingYear;
    private String[] authors;

    public PublishingInfo(String name, int publishingYear, String[] authors) {
        this.organizationName = name;
        this.publishingYear = publishingYear;
        this.authors = authors;
    }

    public String[] getAuthors() {
        return authors;
    }

    public int getPublishingYear() {
        return publishingYear;
    }

    @Override
    public String toString() {
        StringBuilder authorStr = new StringBuilder();
        for (int i = 0; i < this.authors.length; i++) {
            if (i > 0) {
                authorStr.append(", " + authors[i]);
            }else{
                authorStr.append(authors[i]);
            }
        }
        return "\tАвтор: " + authorStr + ";\n" +
                "\tГод издательства: " + this.publishingYear + ";\n" +
                "\tИздательство: " + this.organizationName + ";\n";
    }
}
